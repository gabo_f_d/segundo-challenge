import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux'
import { configureStore } from '@reduxjs/toolkit'
import productReducer from './features/product/productSlice'
import SimpleReactLightbox from 'simple-react-lightbox'
import './index.css'


const store = configureStore({
  reducer: {
    product: productReducer,
  }
})

ReactDOM.render(
  <Provider store={store}>
    <SimpleReactLightbox>
      <App />
    </SimpleReactLightbox>
  </Provider>,
  document.getElementById('root')
);
