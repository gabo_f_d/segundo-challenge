import React from 'react';
import styled from 'styled-components';
import styles from './SideNav.module.css'

const Ul = styled.ul`
  margin-top: -45px;
  list-style: none;
  display: flex;
  flex-flow: row nowrap;
  a {
    padding: 18px 10px;
  }
  @media (max-width: 768px) {
    flex-flow: column nowrap;
    background-color: white;
    position: fixed;
    transform: ${({ open }) => open ? 'translateX(0)' : 'translateX(-100%)'};
    top: 0;
    left: 0;
    height: 100vh;
    width: 300px;
    padding-top: 3.5rem;
    transition: transform 0.3s ease-in-out;  
    li {
      color: black;
    }
  }
`;

const SideNav = ({ open }) => {
  return (
    <Ul open={open} className={styles.sideNv}>

      <a>Collections</a>
      <a>Men</a>
      <a>Women</a>
      <a>About</a>
      <a>Contact</a>
    </Ul>
  )
}

export default SideNav