import React from 'react'
import styles from './Cart.module.css'


function Cart({ carrito, product }) {
    return (
        <div className={styles.superContainer}>
            <div className={styles.cartContainer}>
                <h3 className={styles.title}>Cart</h3>
                <hr className={styles.hrCart} />

                {
                    carrito.item !== 0 ? (
                        <div>
                            <div className={styles.itemBox}>
                                <img className={styles.cartThumbnail} src="../assets/images/image-product-1-thumbnail.jpg" alt="" />
                                <p className={styles.itemData}>{product.name}</p>
                                <button className={styles.delete} onClick={() => carrito.item = 0}>
                                    <img src="../assets/images/icon-delete.svg" alt="" />
                                </button>
                            </div>
                            <p className={styles.itemData}>${carrito.price} x {carrito.item} ${carrito.total}</p>
                            <div className={styles.info}>
                                <button className={styles.btnCheckout}>Checkout</button>
                            </div>
                        </div>

                    ) : (
                        <h4 className={styles.emptyCart}>Your cart is empty.</h4>
                    )
                }

            </div>
        </div>
    )
}

export default Cart
