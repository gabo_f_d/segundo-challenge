import React, { useEffect,useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getProduct } from '../features/product/productSlice'
import NavBar from './NavBar/NavBar'
import MainBody from './MainBody/MainBody'
import styles from './Product.module.css'


function Product() {
    const dispatch = useDispatch()
    const product = useSelector(state => state.product.list)
    const imageUrl = useSelector(state => state.product.list.images? state.product.list.images[0] : "");
    const [items, setItems] = useState(0)
    const [carrito, setCarrito] = useState({})

    useEffect(() => {
        dispatch(getProduct());
    }, [])
    
    console.log(product)

    return (
        <div className={styles.example}>
            <NavBar carrito={carrito} product={product}/>
            <MainBody image={imageUrl} product={product} item={items} setItems={setItems} carrito={carrito} setCarrito={setCarrito}/>
        </div>
    )
}

export default Product
