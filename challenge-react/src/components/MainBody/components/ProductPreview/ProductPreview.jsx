import React, { useState } from 'react'
import styles from './ProductPreview.module.css'
import { SRLWrapper } from "simple-react-lightbox"
import { useLightbox } from 'simple-react-lightbox'


function ProductPreview() {
    const images = ["image-product-1.jpg", "image-product-2.jpg", "image-product-3.jpg", "image-product-4.jpg"]
    const [selectedImage, setSelectedImage] = useState(`../assets/images/${images[0]}`)
    const { openLightbox, closeLightbox } = useLightbox()
    const handleSelectImage = (indexImg) => {
        openLightbox(indexImg)
    }
    const handleHoverImage = (imageUrl) => {
        setSelectedImage(`../assets/images/${imageUrl}`)
    }

    const elements = images.map((imageUrl) => ({ src: `../assets/images/${imageUrl}` }))

    return (
        <div className={styles.imgPreContainer}>
            <SRLWrapper elements={elements} />

            <img 
                className={styles.previewImg}
                src={selectedImage}
                alt="" />

            <div className={styles.thumbContainer}>
                {images.map((imageUrl, index) => <img
                    onClick={() => handleSelectImage(index)}
                    onMouseOver={() => handleHoverImage(imageUrl)}
                    className={styles.thumbnail}
                    src={`../assets/images/${imageUrl.slice(0, -4)}-thumbnail.jpg`}
                    alt='' 
                    />
                    
                    )}
            </div>
        </div>
    )
}

export default ProductPreview
