import React from 'react'
import DataProduct from './components/DataProduct/DataProduct'
import ProductPreview from './components/ProductPreview/ProductPreview'
import styles from './MainBody.module.css'


function MainBody({image,product,item,setItems, carrito, setCarrito}) {

    return (
        <div className={styles.container}>
            <ProductPreview imageUrl={image}/>
            <DataProduct product={product} item={item} setItem={setItems} carrito={carrito} setCarrito={setCarrito}/>
        </div>
    )
}

export default MainBody
