import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const productSlice = createSlice({
    name: 'product',
    initialState: {
        list: [],
    },
    reducers: {
        setProduct : (state, action) => {
            state.list = action.payload;
        },
    },

});

export const { setProduct } = productSlice.actions;

export default productSlice.reducer;

export const getProduct = () => (dispatch) => {
    axios
        .get('https://www.mockachino.com/b045b644-d886-4e/products/7d6f7710-95d0-4a27-ae6c-b02c6cb0348f')
        .then((response) => {
            dispatch(setProduct(response.data))
        })
        .catch((error) => console.log(error));
}
